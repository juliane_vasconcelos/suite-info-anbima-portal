package br.com.galgo.testes.suite;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.cancelar.portal.CancelarInformacaoAnbimaPortal;
import br.com.galgo.consultar.portal.ConsultarInformacaoAnbimaPortal;
import br.com.galgo.enviar.portal.EnviarInformacaoAnbimaPortal;
import br.com.galgo.reenviar.portal.ReenviarInfoAnbimaPortal;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.suite.StopOnFirstFailureSuite;
import br.com.galgo.testes.recursos_comuns.utils.SuiteUtils;

@RunWith(StopOnFirstFailureSuite.class)
@Suite.SuiteClasses({ EnviarInformacaoAnbimaPortal.class,//
		ConsultarInformacaoAnbimaPortal.class,//
		CancelarInformacaoAnbimaPortal.class, //
		ReenviarInfoAnbimaPortal.class,//
		ConsultarInformacaoAnbimaPortal.class })
public class SuiteInfoAnbimaPortal {

	private static final String PASTA_SUITE = "InfoAnbimaPortal";

	@BeforeClass
	public static void setUp() throws Exception {
		SuiteUtils.configurarSuiteDefault(Ambiente.HOMOLOGACAO, PASTA_SUITE);
	}
}
